from __future__ import unicode_literals
from django.db import models
from datetime import datetime
# Create your models here.

class Localidades(models.Model):
	nombre=models.CharField(max_length=100)
	codigopostal=models.IntegerField()
	def __str__(self):
		return self.nombre

class categorias (models.Model):
	nombre=models.CharField(max_length=50)
	def __str__(self):
		    return self.nombre


#class TiposPubs (models.Model):
#	tipo_publicacion=models.CharField(max_length=1)##F para foros y P para publicaciones

#	def __str__(self):
#		return self.tipo_publicacion

class Publicaciones (models.Model):
	usuario=models.ForeignKey("auth.User")
	titulo= models.CharField(max_length=100)
	contenido= models.TextField()
	imagen=models.ImageField(upload_to='/static/img',null=True , blank=True)
	fecha=models.DateTimeField(default=datetime.now)
	localidad=models.ForeignKey("Localidades",null=True)
	direccion=models.CharField(max_length=100,null=True)
	estado=models.CharField(max_length=1, default="C")
	categoria= models.ForeignKey("categorias", null=True, blank=True)
	#tipo_publicacion=models.ForeignKey("TiposPubs")
	tipo_publicacion=models.CharField(max_length=1)##F para foros y P para publicaciones

	def __str__(self):
	    return self.titulo

class Comentarios (models.Model):
	usuario=models.ForeignKey("auth.User")
	publicaciones=models.ForeignKey("Publicaciones")
	comentario=models.TextField(max_length=200)
	fecha=models.DateTimeField(default=datetime.now)


	def __str__(self):
	    return self.comentario

class ClaseUsuario(models.Model):
	Nombre= models.CharField(max_length=30)

	def __str__(self):
		return self.Nombre

class Perfiles(models.Model):
	usuario=models.ForeignKey("auth.User")
	nombre= models.CharField(max_length=100, null=True)
	localidad=models.ForeignKey("Localidades", null=True, blank=True)
	claseuser=models.ForeignKey("ClaseUsuario",null=True, verbose_name="Clase de usuario")
	fechanacimiento=models.DateTimeField(null=True,blank= True, verbose_name="Fecha de nacimiento")
	categoria= models.ForeignKey("categorias", null=True, blank=True)
	estado=models.CharField(max_length=25, null=True,blank=True)
	descripcion=models.TextField(null=True, blank= True)
	foto=models.ImageField(upload_to='img',null=True , blank=True)
	config= models.NullBooleanField(default= False , null=True, blank= True) 


class baneos(models.Model):
	usuario=models.ForeignKey("auth.User")
	tipo=models.CharField(max_length= 1)
	publicacion= models.ForeignKey("publicaciones",null=True, blank=True)
	Comentario= models.ForeignKey("Comentarios", null=True, blank=True)

	def __str__ (self):
		return self.tipo