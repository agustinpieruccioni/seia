from django.shortcuts import render
from django.conf import settings
from django.shortcuts import redirect
from web.forms import FormPublicaciones,FormComentarios, FormPerfiles
from web.models import Publicaciones,categorias,Comentarios, baneos, Perfiles
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q
import pdb;


lisP=[]
lisC=[]

def inicio(request):
    return render(request, 'index.html', {})


def eliminarpublicacionprincipal(request, pk):
    com=Comentarios.objects.filter(pk=pk)
    com.delete()
    return HttpResponseRedirect('/logueado/')


def eliminarpostprincipal(request, pk):
    pub=Publicaciones.objects.filter(pk=pk)
    pub.delete()
    return HttpResponseRedirect('/logueado/')


def eliminarpublicacionforo(request, pk, idpub):
    com=Comentarios.objects.filter(pk=pk)
    com.delete()
    return HttpResponseRedirect('/discusion/' + str(idpub))


def eliminarpostforo(request, pk):
    pub=Publicaciones.objects.filter(pk=pk)
    pub.delete()
    return HttpResponseRedirect('/foro/')


def eliminarpublicacionpuntual(request, pk, idpub):
    com=Comentarios.objects.filter(pk=pk)
    com.delete()
    return HttpResponseRedirect('/publicacion/' + str(idpub))


def eliminarpostpuntual(request, pk):
    pub=Publicaciones.objects.filter(pk=pk)
    pub.delete()
    return HttpResponseRedirect('/logueado/')


def foro(request):
    discusion=Publicaciones.objects.filter(tipo_publicacion='F').order_by('-fecha')
    return render(request, 'foro.html', {"lista_publicaciones":discusion})



def logueado(request):
    if request.user.is_authenticated():

        perfil= Perfiles.objects.filter(usuario=request.user)
        cantidad= Perfiles.objects.filter(usuario=request.user).count()

        if request.method == "GET":
             form_perfil = FormPerfiles()
        else:
            form_perfil =FormPerfiles(request.POST)
            if form_perfil.is_valid():
                perfil = form_perfil.save(commit=False)
                perfil.usuario= request.user
                perfil.config = False
                perfil.save()   
                    
        cate=categorias.objects.all()
        com =Comentarios.objects.all()
        ##cate=categorias.objects.get(nombre='Cumbia')
        ##publicaciones2= cate.publicaciones_set.filter(tipo_publicacion="P").order_by('-fecha')
        #if cantidad > 0 :
        #    if perfil[0].claseuser.Nombre=='Banda':
        #        publicaciones2=Publicaciones.objects.filter(Q(tipo_publicacion='P') & Q(usuario=request.user)).order_by('-fecha')
        #    else:
        #        publicaciones2=Publicaciones.objects.filter(tipo_publicacion='P').order_by('-fecha')
        #else:
        publicaciones2=Publicaciones.objects.filter(tipo_publicacion='P').order_by('-fecha')

        

        permiso= baneos.objects.filter(usuario=request.user, tipo= "C").count()
        if permiso < 5:
            pub=Publicaciones.objects.filter(titulo= request.POST.get('publicacion',''))
           
            if request.method == "GET":
                form_comentario = FormComentarios()

               
            else:
                form_comentario = FormComentarios(request.POST)
                if form_comentario.is_valid():
                    Comentarios2 = form_comentario.save(commit=False)
                    Comentarios2.usuario = request.user
                    for pus in pub:
                        Comentarios2.publicaciones = pus
                    Comentarios2.save()
                    return HttpResponseRedirect('/logueado/')

            return render(request, 'logueado.html', {"lista_publicaciones": publicaciones2, 'form_comentario': form_comentario, "lista_comentarios": com, "lista_categorias": cate, "per": perfil, 'form_perfiles':form_perfil,})
        else:
            if request.method == "POST":
                 return HttpResponse('No puede realizar comentarios')

            return render(request, 'logueado.html', {"lista_publicaciones": publicaciones2, "lista_comentarios": com, "lista_categorias": cate,})

    else:
        return redirect('/accounts/login/')

   

def filtrar(request, tipo):
    if request.user.is_authenticated():
        publi=Publicaciones.objects.filter(categoria=tipo)
        com =Comentarios.objects.all()
        cate=categorias.objects.all()

        permiso= baneos.objects.filter(usuario=request.user, tipo= "C").count()
        if permiso < 5:
            
            pub=Publicaciones.objects.filter(titulo= request.POST.get('publicacion',''))
            if request.method == "GET":
                form_comentario = FormComentarios()

            else:
                form_comentario = FormComentarios(request.POST)
                if form_comentario.is_valid():
                    Comentarios2 = form_comentario.save(commit=False)
                    Comentarios2.usuario = request.user
                    for pus in pub:
                        Comentarios2.publicaciones = pus
                    Comentarios2.save()
                    return HttpResponseRedirect('/logueado/')

            return render(request, 'logueado.html', {"lista_publicaciones": publi, 'form_comentario': form_comentario, "lista_comentarios": com, "lista_categorias": cate,})
        else:
            if request.method == "POST":
                return HttpResponse('No puede realizar comentarios')
            return render(request, 'logueado.html', {"lista_publicaciones": publi, "lista_comentarios": com, "lista_categorias": cate,})
    else:
        return redirect('/accounts/login/')


def publicar(request):
    
    permiso= baneos.objects.filter(usuario=request.user, tipo= "P").count()
    if permiso < 5:
        if request.method == "GET":
            mi_form = FormPublicaciones()

        else:
            mi_form = FormPublicaciones(request.POST, request.FILES or None)
            if mi_form.is_valid():
                Publicaciones = mi_form.save(commit=False)
                Publicaciones.usuario = request.user
                Publicaciones.save()
                return HttpResponseRedirect('/logueado/')

        return render(request, 'publicar.html', {'form_post': mi_form})
    else:
        return HttpResponse('No puede realizar Publicaciones temporalmente')


def reportar(request,idu):
    if (idu,request.user) not in lisP:
        lisP.append((idu,request.user))
        public=Publicaciones.objects.get(pk= idu)
        b=baneos(usuario=public.usuario,
                    tipo="P",
                    publicacion=public,
                    )
        b.save()

        return HttpResponseRedirect('/logueado/')
    else:
        return HttpResponse('Ya lo has denunciado en esta publicacion')



def reportarcomentarios(request,idu):
    if (idu,request.user) not in lisC:
        lisC.append((idu,request.user))
        public=Comentarios.objects.get(pk= idu)
        b=baneos(usuario=public.usuario,
                    tipo="C",
                    Comentario=public,
                    )
        b.save()

        return HttpResponseRedirect('/logueado/')
    else:
        return HttpResponse('Ya has denunciado este comentario anteriormente')

def perfil(request,iduser):
    usuario = Perfiles.objects.filter(usuario=iduser)
    publicaciones = Publicaciones.objects.filter(usuario=iduser)
    comentarios = Comentarios.objects.filter(usuario=iduser)
    for i in usuario:
        return render(request, 'perfil.html', {'perfilusuario': usuario,'pubusuario': publicaciones,'comusuario': comentarios,})
            
def discusion(request,iddisc):
    if request.user.is_authenticated():
        disc = Publicaciones.objects.filter(pk=iddisc)
        comdisc = Comentarios.objects.all()
        cate = categorias.objects.all()
        publicaciones2 = Publicaciones.objects.all().order_by('-fecha')

        permiso= baneos.objects.filter(usuario=request.user, tipo= "C").count()
        if permiso < 5:
            pub=Publicaciones.objects.filter(titulo= request.POST.get('publicacion',''))
           
            if request.method == "GET":
                form_comentario = FormComentarios()

            else:
                form_comentario = FormComentarios(request.POST)
                if form_comentario.is_valid():
                    Comentarios2 = form_comentario.save(commit=False)
                    Comentarios2.usuario = request.user
                    for pus in pub:
                        Comentarios2.publicaciones = pus
                    Comentarios2.save()
                    return HttpResponseRedirect('/discusion/' + str(iddisc))

            return render(request, 'discusion.html', {"lista_publicaciones": publicaciones2, 'form_comentario': form_comentario, 'id_disc': disc, "lista_comentarios": comdisc, "lista_categorias": cate,})
        else:
            if request.method == "POST":
                 return HttpResponse('No puede realizar comentarios')

            return render(request, 'discusion.html', {"lista_publicaciones": publicaciones2, 'id_disc': disc, "lista_comentarios": comdisc, "lista_categorias": cate,})

def publicacion(request,idpub):
    if request.user.is_authenticated():
        publi=Publicaciones.objects.filter(pk=idpub)
        com =Comentarios.objects.all()
        cate=categorias.objects.all()

        permiso= baneos.objects.filter(usuario=request.user, tipo= "C").count()
        if permiso < 5:
            
            pub=Publicaciones.objects.filter(titulo= request.POST.get('publicacion',''))
            if request.method == "GET":
                form_comentario = FormComentarios()

            else:
                form_comentario = FormComentarios(request.POST)
                if form_comentario.is_valid():
                    Comentarios2 = form_comentario.save(commit=False)
                    Comentarios2.usuario = request.user
                    for pus in pub:
                        Comentarios2.publicaciones = pus
                    Comentarios2.save()
                    return HttpResponseRedirect('/publicacion/' + str(idpub))

            return render(request, 'publicacion.html', {"publicaciones": publi, 'form_comentario': form_comentario, "lista_comentarios": com, "lista_categorias": cate,})
        else:
            if request.method == "POST":
                return HttpResponse('No puede realizar comentarios')
            return render(request, 'publicacion.html', {"publicaciones": publi, "lista_comentarios": com, "lista_categorias": cate,})
    else:
        return redirect('/accounts/login/')