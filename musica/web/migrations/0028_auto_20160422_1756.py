# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-22 20:56
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('web', '0027_auto_20160422_1725'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comentarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.TextField(max_length=200)),
                ('fecha', models.DateTimeField(default=datetime.datetime(2016, 4, 22, 17, 56, 50, 748000))),
            ],
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 22, 17, 56, 50, 746000)),
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='imagen',
            field=models.ImageField(null=True, upload_to='/static/img'),
        ),
        migrations.AddField(
            model_name='comentarios',
            name='publicaciones',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.Publicaciones'),
        ),
        migrations.AddField(
            model_name='comentarios',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
