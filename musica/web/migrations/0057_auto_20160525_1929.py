# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-25 22:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0056_auto_20160525_1918'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comentarios',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 25, 19, 29, 4, 194000)),
        ),
        migrations.AlterField(
            model_name='perfiles',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='static\x07dmin\\img'),
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 25, 19, 29, 4, 192000)),
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='static\x07dmin\\img'),
        ),
    ]
