# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-24 17:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0052_auto_20160524_1113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perfiles',
            name='observaciones',
        ),
        migrations.AddField(
            model_name='perfiles',
            name='descripcion',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='perfiles',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='/static/img'),
        ),
        migrations.AlterField(
            model_name='comentarios',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 24, 14, 1, 1, 763000)),
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 24, 14, 1, 1, 761000)),
        ),
    ]
