# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-26 14:47
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0066_auto_20160525_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comentarios',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 26, 11, 47, 8, 404000)),
        ),
        migrations.AlterField(
            model_name='publicaciones',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 26, 11, 47, 8, 404000)),
        ),
    ]
