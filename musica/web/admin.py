from django.contrib import admin
from web.models import Localidades,Publicaciones,Perfiles,categorias,Comentarios,baneos,ClaseUsuario
# Register your models here.

class AdminLocalidad(admin.ModelAdmin):
    list_display = ('id','nombre', 'codigopostal',)
    search_fields = ('nombre', )

admin.site.register(Localidades, AdminLocalidad)

class AdminPublicaciones(admin.ModelAdmin):
    list_display = ('id','usuario', 'titulo', 'fecha',)
    list_filter = ('usuario','titulo', 'fecha')
    search_fields = ('texto', )
    date_hierarchy = 'fecha'

admin.site.register(Publicaciones, AdminPublicaciones)


class AdminPerfiles(admin.ModelAdmin):
    list_display = ('id','usuario', 'estado', 'fechanacimiento',)
    list_filter = ('usuario',)


admin.site.register(Perfiles, AdminPerfiles)
class AdminCategorias(admin.ModelAdmin):
    list_display = ('id','nombre',)
    search_fields = ('nombre', )

admin.site.register(categorias, AdminCategorias)

class Admincomentario(admin.ModelAdmin):
    list_display = ('id','comentario',)
    search_fields = ('comentario', )

admin.site.register(Comentarios, Admincomentario)

class Adminbaneos(admin.ModelAdmin):
    list_display = ('id','usuario','tipo')
    search_fields = ('usuario', )

admin.site.register(baneos, Adminbaneos)


class AdminClase(admin.ModelAdmin):
    list_display = ('id','Nombre',)
    search_fields = ('Nombre',)

admin.site.register(ClaseUsuario, AdminClase)

#class AdminTipos(admin.ModelAdmin):
 #   list_display = ('id','tipo_publicacion',)
 #   search_fields = ('tipo_publicacion', )

#admin.site.register(TiposPubs, AdminTipos)