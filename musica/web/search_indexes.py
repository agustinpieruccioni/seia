from haystack import indexes
from web.models import Publicaciones,Comentarios
from django.core.urlresolvers import reverse


class PublicacionesIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	titulo = indexes.CharField(model_attr="titulo", use_template=True)
	contenido = indexes.CharField(model_attr="contenido", use_template=True)

	def get_model(self):
		return Publicaciones

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.all()

	def get_absolute_url(self):
		return reverse('web.views.publicacion', args=[self.id])

class ComentariosIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	comentario = indexes.CharField(model_attr="comentario", use_template=True)

	def get_model(self):
		return Comentarios

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.all()

	def get_absolute_url(self):
		return urlresolvers.reverse('get_absolute_url', args=[self.id])