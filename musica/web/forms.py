from django import forms
from web.models import Publicaciones,Comentarios,categorias,Perfiles
from django.utils.translation import ugettext_lazy as _

class FormattedDateField(forms.DateField):
	widget = forms.DateInput(format='%m/%d/%Y')

	def __init__(self, *args, **kwargs):
		super(FormattedDateField, self).__init__(*args, **kwargs)
		self.input_formats = ('%m/%d/%Y',)


class FormPublicaciones(forms.ModelForm):
    class Meta:
        model = Publicaciones
        exclude = ['usuario','estado','fecha']

class FormComentarios(forms.ModelForm):
    class Meta:
        model= Comentarios
        exclude = ['usuario','publicaciones','fecha']

class FormPerfiles(forms.ModelForm):
	fechanacimiento = FormattedDateField()
	class Meta:
		model=Perfiles
		exclude=['usuario','config','observaciones','estado']