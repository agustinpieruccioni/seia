"""musica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import *
from django.contrib import admin




urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^$', 'web.views.inicio'),
    url(r'^logueado/', 'web.views.logueado'),
    url(r'^publicar/$', 'web.views.publicar'),
    url(r'^eliminarprincipal/(?P<pk>[0-9]+)/$', 'web.views.eliminarpublicacionprincipal'),
    url(r'^eliminarpubprincipal/(?P<pk>[0-9]+)/$', 'web.views.eliminarpostprincipal'),
    url(r'^discusion/(?P<pk>[0-9]+)/(?P<idpub>[0-9]+)/$', 'web.views.eliminarpublicacionforo'),
    url(r'^eliminarpubforo/(?P<pk>[0-9]+)/$', 'web.views.eliminarpostforo'),
    url(r'^eliminarcompuntual/(?P<pk>[0-9]+)/(?P<idpub>[0-9]+)/$', 'web.views.eliminarpublicacionpuntual'),
    url(r'^eliminarpubpuntual/(?P<pk>[0-9]+)/$', 'web.views.eliminarpostpuntual'),
    url(r'^foro/$', 'web.views.foro'),
    url(r'^filtro/(?P<tipo>[0-9]+)/$', 'web.views.filtrar'),
    url(r'^denunciar/(?P<idu>[0-9]+)/$', 'web.views.reportar'),
    url(r'^denunciarcomen/(?P<idu>[0-9]+)/$', 'web.views.reportarcomentarios'),
    url(r'^perfil/(?P<iduser>[0-9]+)/$', 'web.views.perfil'),
    url(r'^discusion/(?P<iddisc>[0-9]+)/$', 'web.views.discusion'),
    url(r'^publicacion/(?P<idpub>[0-9]+)/$', 'web.views.publicacion'),
    url(r'^search/', include('haystack.urls')),
]
